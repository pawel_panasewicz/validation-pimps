/**
 * Paweł Panasewicz pawel.panasewicz@panasoft.pl
 */

import sbt._
import sbt.Keys._

object PimpsBuildPlugins extends Build {

  lazy val root = Project("BuildPlugins", file(".")).settings(
    resolvers += "Sonatype snapshots" at "http://oss.sonatype.org/content/repositories/snapshots/",
    resolvers += "sbt-plugin-releases" at "http://scalasbt.artifactoryonline.com/scalasbt/simple/sbt-plugin-releases/",
    resolvers += "sbt-plugin-snapshots" at "http://scalasbt.artifactoryonline.com/scalasbt/simple/sbt-plugin-snapshots/",
    resolvers += "plugins-releases" at "http://scalasbt.artifactoryonline.com/scalasbt/simple/plugins-releases",
    addSbtPlugin("com.github.mpeltonen" % "sbt-idea" % "1.2.0-SNAPSHOT"),
    addSbtPlugin("com.typesafe.sbteclipse" % "sbteclipse-plugin" % "2.1.0"),
    addSbtPlugin("com.typesafe.sbt" % "sbt-pgp" % "0.7")
  )

}