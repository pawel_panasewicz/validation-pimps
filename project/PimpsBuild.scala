/**
 * Paweł Panasewicz pawel.panasewicz@panasoft.pl
 */

import sbt._
import sbt.Keys._
import scala.Some

object PimpsBuild extends Build {

  lazy val root =
    Project("validation-pimps", file("."))
      .settings(PublishingSettings.publishingSettings: _*)
      .settings(
       version := "0.2.4-SNAPSHOT",
      organization := "pl.panasoft",
      organizationName := "PANASOFT",
      scalaVersion := "2.10.0",
      scalacOptions += "-feature",
      libraryDependencies ++= "org.scalatest" %% "scalatest" % "2.0.M5b" % "test"
        :: "org.scalaz" %% "scalaz-core" % "7.0.0-M7"
        :: "org.slf4j" % "slf4j-api" % "1.6.6"
        :: "com.weiglewilczek.slf4s" % "slf4s_2.9.1" % "1.0.7"
        :: "ch.qos.logback" % "logback-classic" % "1.0.6"
        :: "ch.qos.logback" % "logback-core" % "1.0.7"
        :: Nil,

      //    D - show durations
      //    S - show short stack traces
      //    F - show full stack traces
      //    W - without color
      testOptions in Test += Tests.Argument("-oDFW"),

      exportJars := true,
      resolvers += "Oss sonatype" at "https://oss.sonatype.org/content/groups/public/",
      resolvers += "Maven central" at "http://repo1.maven.org/maven2/",
      resolvers += "Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/"
    )
      .settings(Defaults.itSettings: _*)
      .configs(Debug)
      .settings(inConfig(Debug)(Defaults.configTasks): _*)
      .settings(
      javaOptions in Debug += "-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=5005",
      fork in Debug := true
    )

  lazy val Debug = config("debug").extend(Test)

  object PublishingSettings {

    private val publishToSetting = publishTo <<= version {
      (v: String) =>
        val nexus = "https://oss.sonatype.org/"
        if (v.trim.endsWith("SNAPSHOT"))
          Some("snapshots" at nexus + "content/repositories/snapshots")
        else
          Some("staging releases" at nexus + "service/local/staging/deploy/maven2")
    }

    private val pomIncludeRepositorySetting = pomIncludeRepository := {
      _ => false
    }

    private val pomExtraSetting = pomExtra := (
      <url>https://bitbucket.org/pawel_panasewicz/validation-pimps</url>
        <licenses>
          <license>
            <name>Apache License Version 2.0</name>
            <url>http://www.apache.org/licenses/LICENSE-2.0</url>
            <distribution>repo</distribution>
          </license>
        </licenses>
        <scm>
          <connection>scm:git:https://pawel_panasewicz@bitbucket.org/pawel_panasewicz/validation-pimps.git</connection>
          <developerConnection>scm:git:https://pawel_panasewicz@bitbucket.org/pawel_panasewicz/validation-pimps.git</developerConnection>
          <url>https://pawel_panasewicz@bitbucket.org/pawel_panasewicz/validation-pimps</url>
        </scm>
        <developers>
          <developer>
            <id>pawel_panasewicz</id>
            <name>Paweł Panasewicz</name>
            <url>http://panasoft.pl</url>
          </developer>
        </developers>)

    val publishingSettings = Seq(
      publishToSetting,
      pomIncludeRepositorySetting,
      pomExtraSetting,
      publishMavenStyle := true,
      publishArtifact in Test := false
    )
  }
}
