This is a Scala library that extends *scalaz*. It adds some nice features to `scalaz.Validation`.

Features

* Comfortable wrapping to `scalaz.Validation`.
* Comfortable verifying resulting in `scalaz.Validation`.
* Comfortable color printing using `scalaz.Show` or `toString`.

Usage
```scala
    libraryDependencies += "pl.panasoft" %% "pimps" % "0.2.3"

    resolvers += "OSS Sonatype snapshots" at "https://oss.sonatype.org/content/repositories/releases/"

    scalaVersion := "2.10.0"
```


Example:

```
  TODO
    String s = "........"

    trimInside(s)
      .split("\n")
      .toList
      .verify((x: List[String]) => s"Data Storage doesn't match expected pattern. List size is not 4 (actual ${x.size}). Text value is \n`${trimInside(node.text)}`.")(_.size === 5)
      .validation
      .verify((x: List[String]) => s"Data Storage doesn't match expected pattern. 2th line must contain digits. Text value is \n`${trimInside(node.text)}`.")(x => containsDigit(x(1)))
      .verify((x: List[String]) => s"Data Storage doesn't match expected pattern. 2th line must contain 'GB' or 'MB' or 'TB' or 'KB' or 'B'. Text value is \n`${trimInside(node.text)}`.")(x => x(1).contains("GB") || x(1).contains("MB")|| x(1).contains("TB") || x(1).contains("KB")|| x(1).contains("B"))
      .verify((x: List[String]) => s"Data Storage doesn't match expected pattern. 2th line have more than 3 characters. Text value is \n`${trimInside(node.text)}`.")(x => x(1).length > 3)
      .verify((x: List[String]) => s"Data Storage doesn't match expected pattern. 4th line must contain 'maximum'. Text value is \n`${trimInside(node.text)}`.")(x => x(3).contains("maximum"))
      .verify((x: List[String]) => s"Data Storage doesn't match expected pattern. 4th line must contain 'GB' or 'MB' or 'TB' or 'KB' or 'B'. Text value is \n`${trimInside(node.text)}`.")(x => x(3).contains("GB") || x(3).contains("MB")|| x(3).contains("TB") || x(3).contains("KB")|| x(3).contains("B"))
      .validation
```