libraryDependencies += "pl.panasoft" %% "pimps" % "0.1-SNAPSHOT"

resolvers += "OSS Sonatype snapshots" at "https://oss.sonatype.org/content/repositories/snapshots/"

scalaVersion := "2.10.0-M7"

scalacOptions += "-feature"
