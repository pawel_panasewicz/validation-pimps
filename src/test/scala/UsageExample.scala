import pimpers._

import scalaz._
import Scalaz._

object UsageExample {

  "abc2".p
  "abc".pRed
  1.success[Int].pMagenta
  "errorX".failure[S].pGreen
  Seq(1, 2, 3, 4).pYellow
  List(1, 2, 3, 4).pCyan

  val data = -200

  data.verify("data must be negative")(_ < 0)
    .verify("data must be GT -1000")(_ > -1000)
    .verify("data must be even")(_ % 2 == 0)
    .validation

  data.success[S].verify("data must be negative")(_ < 0)
    .verify("data must be GT -1000")(_ > -1000)
    .verify("data must be even")(_ % 2 == 0)
    .validation
}
