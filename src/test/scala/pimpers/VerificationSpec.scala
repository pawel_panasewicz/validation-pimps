/*
* Copyright (c) 2012 Paweł Panasewicz
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/

package pimpers

import org.scalatest.FreeSpec
import scalaz._
import Scalaz._

class VerificationSpec extends FreeSpec {

  "A scala expression " - {
    "can be validated using convenient syntax" - {
      "Verification can be converted to scalaz.SuceessNel if success" in {

        val data = -200

        data.verify("data must be negative")(_ < 0)
          .verify("data must be GT -1000")(_ > -1000)
          .verify("data must be even")(_ % 2 == 0)
          .validation assert_=== -200.successNel[String]
      }
      "Verification can be converted to scalaz.FailureNel if failures" in {

        val data = 101

        data.verify("data must be negative")(_ < 0)
          .verify("data must be GT -1000")(_ > -1000)
          .verify("data must be even")(_ % 2 == 0)
          .validation assert_=== NonEmptyList("data must be negative", "data must be even").failure[Int]
      }
      "Verification e message can tested value" in {

        val data = 101

        val validation: Validation[NonEmptyList[String], Int] = NonEmptyList("data must be negative. Current value is 101.", "data must be even. Current value is 101.").failure[Int]

        data.verify((i: Int) => s"data must be negative. Current value is ${i}.")(_ < 0)
          .verify((i: Int) => s"data must be GT -1000. Current value is ${i}.")(_ > -1000)
          .verify((i: Int) => s"data must be even. Current value is ${i}.")(_ % 2 == 0)
          .validation assert_=== validation
      }
    }
    "result of validated expression is type of ValidationA" in {

      -123.verify("data must be negative")(_ < 0).isInstanceOf[VerificationA[String, Int]] assert_=== true
      123.verify("data must be negative")(_ < 0).isInstanceOf[VerificationA[String, Int]] assert_=== true
    }
  }

  "A scala expression resulting of scalaz.Validation" - {
    "can be validated as well using similar convenient syntax" - {
      "Verification can be converted to scalaz.SuceessNel if success" in {

        val data = -200.success[String]

        data.verify("data must be negative")(_ < 0)
          .verify("data must be GT -1000")(_ > -1000)
          .verify("data must be even")(_ % 2 == 0)
          .validation assert_=== -200.successNel[String]
      }
      "Verification can be converted to scalaz.FailureNel if failures" in {

        val data = 101.success[String]

        data.verify("data must be negative")(_ < 0)
          .verify("data must be GT -1000")(_ > -1000)
          .verify("data must be even")(_ % 2 == 0)
          .validation assert_=== NonEmptyList("data must be negative", "data must be even").failure[Int]
      }
      "If Validation is failure, it is not validated" in {
        "ERROR".failure[Int]
          .verify("data must be positive")(_ > 0)
          .verify("data must be GT -100")(_ > -100)
          .verify("data must be GT 2")(_ > 2)
          .validation assert_=== "ERROR".failureNel[Int]
      }
      "Verification e message can tested value" in {

        val data = -200.success[String]

        data.verify("Data must be negative")(_ < 0)
          .verify(x => s"Data must be GT -1000. Current value is ${x}.")(_ > -1000)
          .verify(x => s"Data must be even. Current value is ${x}.")(_ % 2 == 0)
          .validation assert_=== -200.successNel[String]

        val data2 = 101.success[String]

        data2.verify(x => s"Data must be negative. Current value is ${x}.")(_ < 0)
          .verify(x => s"Data must be GT -1000. Current value is ${x}.")(_ > -1000)
          .verify(x => s"Data must be even. Current value is ${x}.")(_ % 2 == 0)
          .validation assert_=== NonEmptyList("Data must be negative. Current value is 101.", "Data must be even. Current value is 101.").failure[Int]

      }
    }
    "result of validated expression is type of ValidationA" in {
      -123.success[String].verify("data must be negative")(_ < 0).isInstanceOf[VerificationV[String, Int]] assert_=== true
      "ERR".failure[Int].verify("data must be negative")(_ < 0).isInstanceOf[VerificationV[String, Int]] assert_=== true
    }

    "A scala expression resulting of scalaz.ValidationNEL" - {
      "can be validated as well using similar convenient syntax" - {
        "Verification can be converted to scalaz.SuceessNel if success" in {
          val data = -200.successNel[String]

          data.verify("data must be negative")(_ < 0)
            .verify("data must be GT -1000")(_ > -1000)
            .verify("data must be even")(_ % 2 == 0)
            .validation assert_=== -200.successNel[String]

        }
        "Verification can be converted to scalaz.FailureNel if failures" in {
          val data = 101.successNel[String]

          data.verify("data must be negative")(_ < 0)
            .verify("data must be GT -1000")(_ > -1000)
            .verify("data must be even")(_ % 2 == 0)
            .validation assert_=== NonEmptyList("data must be negative", "data must be even").failure[Int]

        }
        "If ValidationNel is Failure, it is not validated" in {

          "ERROR".failureNel[Int]
            .verify("data must be positive")(_ > 0)
            .verify("data must be GT -100")(_ > -100)
            .verify("data must be GT 2")(_ > 2)
            .validation assert_=== "ERROR".failureNel[Int]
        }
        "Result of validated expression is type of ValidationVNel" in {

          -123.successNel[String].verify("data must be negative")(_ < 0).isInstanceOf[VerificationVNel[String, Int]] assert_=== true
          "ERRO".failureNel[Int].verify("data must be negative")(_ < 0).isInstanceOf[VerificationVNel[String, Int]] assert_=== true
        }
        "Verification e message can tested value" in {
          val data = 101.successNel[String]

          data.verify(x=> s"Data must be negative. Current value is ${x}.")(_ < 0)
            .verify(x=> s"Data must be GT -1000. Current value is ${x}.")(_ > -1000)
            .verify(x=> s"Data must be even. Current value is ${x}.")(_ % 2 == 0)
            .validation assert_=== NonEmptyList("Data must be negative. Current value is 101.", "Data must be even. Current value is 101.").failure[Int]
        }
      }
    }
  }
}

