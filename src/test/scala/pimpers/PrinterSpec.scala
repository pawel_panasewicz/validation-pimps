/*
* Copyright (c) 2012 Paweł Panasewicz
*
*    Licensed under the Apache License, Version 2.0 (the "License");
*    you may not use this file except in compliance with the License.
*    You may obtain a copy of the License at
*
*        http://www.apache.org/licenses/LICENSE-2.0
*
*    Unless required by applicable law or agreed to in writing, software
*    distributed under the License is distributed on an "AS IS" BASIS,
*    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*    See the License for the specific language governing permissions and
*    limitations under the License.
*/

package pimpers

import org.scalatest.FreeSpec
import scalaz._
import Scalaz._
import java.io.{ByteArrayOutputStream, OutputStream, PrintStream}
import language.reflectiveCalls

class PrinterSpec extends FreeSpec {


  "A Printer" ignore {
    "should println corresponsing value using implicit PrintStream" in {

      val os1 = os
      implicit def out: PrintStream = new PrintStream(os1)
      val expected =
        """abc
          |Success(1)
          |Failure(errorX)
          |List(1, 2, 3, 4)""".stripMargin

      "abc".p
      1.success[Int].p
      "errorX".failure[S].p
      Seq(1, 2, 3, 4).p

      os1.sb.toString().trim assert_=== expected
      os1.sb.clear()
    }

    "should color println corresponsing value" in {
      val os1 = os
      implicit def out: PrintStream = new PrintStream(os1)
      "abc".pRed
      1.success[Int].pMagenta
      "errorX".failure[S].pGreen
      Seq(1, 2, 3, 4).pYellow
      List(1, 2, 3, 4).pCyan
    }
  }

  private def os = new OutputStream {
    val sb = StringBuilder.newBuilder
    def write(b: Int) = sb.append(b.asInstanceOf[Char])
  }
}
