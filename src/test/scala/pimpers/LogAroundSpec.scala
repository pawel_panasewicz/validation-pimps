/*
* Copyright (c) 2012 Paweł Panasewicz
*
*    Licensed under the Apache License, Version 2.0 (the "License");
*    you may not use this file except in compliance with the License.
*    You may obtain a copy of the License at
*
*        http://www.apache.org/licenses/LICENSE-2.0
*
*    Unless required by applicable law or agreed to in writing, software
*    distributed under the License is distributed on an "AS IS" BASIS,
*    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*    See the License for the specific language governing permissions and
*    limitations under the License.
*/

package pimpers

import org.scalatest.FreeSpec
import scalaz._
import Scalaz._

/**
 * Paweł Panasewicz, pawel.panasewicz@panasoft.pl
 */
class LogAroundSpec extends FreeSpec {


  "A LogAround" ignore {
    "should log computation subject around any scala expression" in {
      1.traceAround("counting 1")
      1.success[String].traceAround("counting sucess(1)")
      1.debugAround("counting 1")
      1.success[String].debugAround("counting sucess(1)")
      1.infoAround("counting 1")
      1.success[String].infoAround("counting sucess(1)")
      1.warnAround("counting 1")
      1.success[String].warnAround("counting sucess(1)")
      1.errorAround("counting 1")
      1.success[String].errorAround("counting sucess(1)")
      "error".fail[Int].traceAround("whatever")
      "error".fail[Int].debugAround("counting 1")
      "error".fail[Int].infoAround("counting 1")
      "error".fail[Int].warnAround("counting 1")
      "error".fail[Int].errorAround("counting 1")
      "error".fail[Int].errorAround(List("counting 1"))
    }

    "should return loggin around scala exression" in {
      1.traceAround("counting 1") assert_=== 1
      1.success[String].traceAround("counting sucess(1)") assert_=== 1.success[String]
      1.debugAround("counting 1") assert_=== 1
      1.success[String].debugAround("counting sucess(1)") assert_=== 1.success[String]
      1.infoAround("counting 1") assert_=== 1
      1.success[String].infoAround("counting sucess(1)") assert_=== 1.success[String]
      1.warnAround("counting 1") assert_=== 1
      1.success[String].warnAround("counting sucess(1)") assert_=== 1.success[String]
      1.errorAround("counting 1") assert_=== 1
      1.success[String].errorAround("counting sucess(1)") assert_=== 1.success[String]
      "error".fail[Int].traceAround("whatever") assert_=== "error".fail[Int]
      "error".fail[Int].debugAround("counting 1") assert_=== "error".fail[Int]
      "error".fail[Int].infoAround("counting 1") assert_=== "error".fail[Int]
      "error".fail[Int].warnAround("counting 1") assert_=== "error".fail[Int]
      "error".fail[Int].errorAround("counting 1") assert_=== "error".fail[Int]
    }

    "should log computation subject and then computation subject with addon function around any scala expression" in {
      1.traceAround("Counting 1.", x => s"Counted ${x}")

      1.success[String].traceAround("counting sucess(1)", x => s"Counted ${x}")
      1.debugAround("counting 1")
      1.success[String].debugAround("counting sucess(1)", x => s"Counted ${x}")
      1.infoAround("counting 1")
      1.success[String].infoAround("counting sucess(1)", x => s"Counted ${x}")
      1.warnAround("counting 1")
      1.success[String].warnAround("counting sucess(1)", x => s"Counted ${x}")
      1.errorAround("counting 1")
      1.success[String].errorAround("counting sucess(1)", x => s"Counted ${x}")
      "error".fail[Int].traceAround("whatever", x => s"Whatever ${x}")
      "error".fail[Int].debugAround("counting 1", x => s"Counted ${x}")
      "error".fail[Int].infoAround("counting 1", x => s"Counted ${x}")
      "error".fail[Int].warnAround("counting 1", x => s"Counted ${x}")
      "error".fail[Int].errorAround("counting 1", x => s"Counted ${x}")
      "error".fail[Int].errorAround(List("counting 1"), x => s"Counted ${x}")
    }

    "should return loggin around scala exression when logging with value" in {
      1.traceAround("counting 1", x => s"Counted ${x}") assert_=== 1
      1.success[String].traceAround("counting sucess(1)", x => s"Counted ${x}") assert_=== 1.success[String]
      1.debugAround("counting 1", x => s"Counted ${x}") assert_=== 1
      1.success[String].debugAround("counting sucess(1)", x => s"Counted ${x}") assert_=== 1.success[String]
      1.infoAround("counting 1", x => s"Counted ${x}") assert_=== 1
      1.success[String].infoAround("counting sucess(1)", x => s"Counted ${x}") assert_=== 1.success[String]
      1.warnAround("counting 1", x => s"Counted ${x}") assert_=== 1
      1.success[String].warnAround("counting sucess(1)", x => s"Counted ${x}") assert_=== 1.success[String]
      1.errorAround("counting 1", x => s"Counted ${x}") assert_=== 1
      1.success[String].errorAround("counting sucess(1)", x => s"Counted ${x}") assert_=== 1.success[String]
      "error".fail[Int].traceAround("whatever", x => s"Counted ${x}") assert_=== "error".fail[Int]
      "error".fail[Int].debugAround("counting 1", x => s"Counted ${x}") assert_=== "error".fail[Int]
      "error".fail[Int].infoAround("counting 1", x => s"Counted ${x}") assert_=== "error".fail[Int]
      "error".fail[Int].warnAround("counting 1", x => s"Counted ${x}") assert_=== "error".fail[Int]
      "error".fail[Int].errorAround("counting 1", x => s"Counted ${x}") assert_=== "error".fail[Int]
    }

    "should return subject when just logging" in {
      1.trace assert_=== 1
      1.success[String].trace assert_=== 1.success[String]
      1.debug assert_=== 1
      1.success[String].debug assert_=== 1.success[String]
      1.info assert_=== 1
      1.success[String].info assert_=== 1.success[String]
      1.warn assert_=== 1
      1.success[String].warn assert_=== 1.success[String]
      1.error assert_=== 1
      1.success[String].error assert_=== 1.success[String]
      "error".fail[Int].trace assert_=== "error".fail[Int]
      "error".fail[Int].debug assert_=== "error".fail[Int]
      "error".fail[Int].info assert_=== "error".fail[Int]
      "error".fail[Int].warn assert_=== "error".fail[Int]
      "error".fail[Int].error assert_=== "error".fail[Int]
    }

    "should log around with function on value" in {

    }

  }
}
