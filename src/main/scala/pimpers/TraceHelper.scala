///*
// * Copyright (c) 2012 Paweł Panasewicz
// *
// *    Licensed under the Apache License, Version 2.0 (the "License");
// *    you may not use this file except in compliance with the License.
// *    You may obtain a copy of the License at
// *
// *        http://www.apache.org/licenses/LICENSE-2.0
// *
// *    Unless required by applicable law or agreed to in writing, software
// *    distributed under the License is distributed on an "AS IS" BASIS,
// *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// *    See the License for the specific language governing permissions and
// *    limitations under the License.
// */
//
//package pimpers
//import scalaz._
//import Scalaz._
//
///**
// * created by: Paweł Panasewicz
// * mail: pawel.panasewicz@panasoft.pl
// */
//object TraceHelper {
//
//    def getCallMethodName(): String = {
//      getCallMethodName(3).fold(identity)
//    }
//
//    def getCallMethodName(depth:Int): Validation[String, String] = {
//      try {
//        val trace: StackTraceElement = Thread.currentThread().getStackTrace()(depth)
//        trace.getMethodName.success
//      }catch{
//        case e:Exception => s"could not find methodName name, enclosing exception is `${e.getMessage}`".fail
//      }
//    }
//}
