///*
// * Copyright (c) 2012 Paweł Panasewicz
// *
// *    Licensed under the Apache License, Version 2.0 (the "License");
// *    you may not use this file except in compliance with the License.
// *    You may obtain a copy of the License at
// *
// *        http://www.apache.org/licenses/LICENSE-2.0
// *
// *    Unless required by applicable law or agreed to in writing, software
// *    distributed under the License is distributed on an "AS IS" BASIS,
// *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// *    See the License for the specific language governing permissions and
// *    limitations under the License.
// */
//
//package pimpers
//
//import scalaz._
//import Scalaz._
//import java.io.File
//import org.springframework.util.AntPathMatcher
//import collection.JavaConversions._
//import collection.JavaConverters._
//import Pimps._
//
///**
// * Paweł Panasewicz, pawel.panasewicz@panasoft.pl
// */
//
//trait SimpleFileFinder{
//
//  def each: FF = f => true
//
//  def currCL = Thread.currentThread().getContextClassLoader
//
//  def find(rootDir: File)(filter: FF): List[File]
//
//  def findFirst(rootDir: File)(filter: FF): Option[File]
//
//  def find(rootDir: File, antStylePattern: String): List[File]
//
//  def findFirst(rootDir: File, antStylePattern: String): Option[File]
//}
//
//object SimpleFileFinder extends SimpleFileFinder {
//
//  def find(rootDir: File)(filter: FF): List[File] = {
//    def files: List[File] = (rootDir.listFiles().toList ?? List()).filter(it => it.isFile && filter(it))
//    def dirs: List[File] = (rootDir.listFiles().toList ?? List()).filter(it => it.isDirectory)
//    def filesInSubDirs: List[File] = dirs.flatMap(find(_)(filter))
//    filesInSubDirs |+| files
//  }
//
//  def findFirst(rootDir: File)(filter: FF): Option[File] =
//    find(rootDir)(filter).toList.headOption
//
//  def FFFromPathPattern(pathPattern: String): FF = {
//    val pathMatcher = new AntPathMatcher()
//    (f: File) => val fs: String = System.getProperty("file.separator")
//    val path: String = f.getPath.replace(fs, "/")
//    val canonicalPath = if (path.startsWith("./")) path.replaceFirst("./", "/") else path
//    val canonicalPathPattern = if (pathPattern.startsWith("/")) pathPattern else "/" + pathPattern
//    pathMatcher.`match`(canonicalPathPattern, canonicalPath)
//  }
//
//  def find(rootDir: File, antStylePattern: String): List[File] = {
//    find(rootDir)(FFFromPathPattern(antStylePattern))
//  }
//
//  def findFirst(rootDir: File, antStylePattern: String): Option[File] = find(rootDir, antStylePattern).headOption
//}
//
//trait FileFinder {
//
//  def find(rootDir: File): List[File]
//
//  def findFirst(rootDir: File): Option[File]
//}
//
//case class FileFinderBuilder(includeFF: List[FF], excludeFF: List[FF]) extends FileFinder {
//
//  def include(ff: FF): FileFinderBuilder = this.copy(includeFF = includeFF :+ ff)
//
//  def exclude(ff: FF): FileFinderBuilder = this.copy(excludeFF = excludeFF :+ ff)
//
//  def include(pathPattern: String): FileFinderBuilder = include(SimpleFileFinder.FFFromPathPattern(pathPattern))
//
//  def exclude(pathPattern: String): FileFinderBuilder = exclude(SimpleFileFinder.FFFromPathPattern(pathPattern))
//
//  def find(rootDir: File) = SimpleFileFinder.find(rootDir)(fileFilters)
//
//  def findFirst(rootDir: File) = SimpleFileFinder.findFirst(rootDir)(fileFilters)
//    def include: FF = (f) => includeFF.exists(_(f))
//
//  private def fileFilters: FF = {
//    def exclude: FF = (f) => excludeFF.exists(_(f))
//    def ff: FF = (f) => include(f) && !exclude(f)
//    ff
//  }
//}
//
//object FileFinderBuilder extends FileFinderBuilder(Nil, Nil)