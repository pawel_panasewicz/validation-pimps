/*
* Copyright (c) 2012 Paweł Panasewicz
*
*    Licensed under the Apache License, Version 2.0 (the "License");
*    you may not use this file except in compliance with the License.
*    You may obtain a copy of the License at
*
*        http://www.apache.org/licenses/LICENSE-2.0
*
*    Unless required by applicable law or agreed to in writing, software
*    distributed under the License is distributed on an "AS IS" BASIS,
*    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*    See the License for the specific language governing permissions and
*    limitations under the License.
*/

package pimpers

import language.implicitConversions
import com.weiglewilczek.slf4s.{Logger, Logging}
import util.Random
import scalaz._
import Scalaz._
import org.slf4j.LoggerFactory


object LogAround {

  def makeStamp = new Random().alphanumeric.take(10).mkString

  def logAround[E, A](a: => A, subject: => E, subjectAddon: A => E, methodName: String, logger: Logger): A = {
    val stamp = makeStamp
    val c = a
    val s = subject
    log(s"[STARTED:$stamp] - ${s}", logger, methodName)

    val addon = if (subjectAddon == null) "" else s" ${subjectAddon(c)}"
    log(s"[FINISHED:$stamp] - ${
      s
    }${addon}", logger, methodName)
    c
  }

  private def log(m: => String, logger: Logger, methodName: String) = methodName match {
    case "trace" => logger.trace(m)
    case "debug" => logger.debug(m)
    case "info" => logger.info(m)
    case "warn" => logger.warn(m)
    case "error" => logger.error(m)
  }
}

trait LogAroundOps[A] extends syntax.Ops[A] {

  implicit def logger: Logger

  def traceAround[E](subject: => E): A = LogAround.logAround(self, subject, null, "trace", logger)

  def debugAround[E](subject: => E): A = LogAround.logAround(self, subject, null, "debug", logger)

  def infoAround[E](subject: => E): A = LogAround.logAround(self, subject, null, "info", logger)

  def warnAround[E](subject: => E): A = LogAround.logAround(self, subject, null, "warn", logger)

  def errorAround[E](subject: => E): A = LogAround.logAround(self, subject, null, "error", logger)

  def traceAround[E](subject: => E, addon: A => E): A = LogAround.logAround(self, subject, addon, "trace", logger)

  def debugAround[E](subject: => E, addon: A => E): A = LogAround.logAround(self, subject, addon, "debug", logger)

  def infoAround[E](subject: => E, addon: A => E): A = LogAround.logAround(self, subject, addon, "info", logger)

  def warnAround[E](subject: => E, addon: A => E): A = LogAround.logAround(self, subject, addon, "warn", logger)

  def errorAround[E](subject: => E, addon: A => E): A = LogAround.logAround(self, subject, addon, "error", logger)

  def trace: A = {
    logger.trace(self.toString)
    self
  }

  def debug: A = {
    logger.debug(self.toString)
    self
  }

  def info: A = {
    logger.info(self.toString)
    self
  }

  def warn: A = {
    logger.warn(self.toString)
    self
  }

  def error: A = {
    logger.error(self.toString)
    self
  }
}

trait ToLogAroundOps {

  implicit val logger = Logger("DefaultLogAround")

  implicit def ToLogAroundOps[A](a: => A)(implicit F: Logger) = new LogAroundOps[A] {
    def self = a

    def logger = F
  }
}

trait ImplicitLogger {
  implicit val logger = Logger(this.getClass)
}