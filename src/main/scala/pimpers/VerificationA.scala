package pimpers

import scalaz._
import Scalaz._
import scala.language.implicitConversions

trait VerificationA[E, A] {

  val errors: List[E]

  val a: A

  def verify(e: => E)(test: Test[A]): VerificationA[E, A] = verify(_ => e)(test)

  def verify(e: A => E)(test: Test[A]) = verifyToErrors(e(a), test) match {
    case Nil => this
    case errors: List[E] => VerificationA[E, A](a, errors)
  }

  def validation: Validation[NonEmptyList[E], A] = errors match {
    case Nil => a.success[NonEmptyList[E]]
    case _ => NonEmptyList.nel(errors.head, errors.tail).fail[A]
  }

  private def verifyToErrors(e: => E, test: Test[A]): List[E] = if (!test(a)) errors |+| List(e) else Nil
}

object VerificationA {

  def apply[E, A](a_ : A) = new VerificationA[E, A] {
    val errors = Nil

    val a = a_
  }

  def apply[E, A](a_ : A, errs: List[E]) = new VerificationA[E, A] {
    val errors = errs

    val a = a_
  }

  implicit def VerificationShow[E: Show, A: Show]: Show[VerificationA[E, A]] = new Show[VerificationA[E, A]] {
    override def shows(f: VerificationA[E, A]): String = "VerificationA(" + f.errors.shows + ", " + f.a.shows + ")"
  }
}

trait VerificationAOps[A] extends syntax.Ops[A] {

  def verify[E](e: A => E): Test[A] => VerificationA[E, A] = (test: Test[A]) => test(self) match {
    case true => VerificationA[E, A](self)
    case false => VerificationA[E, A](self, List(e(self)))
  }

  def verify[E](e: => E): Test[A] => VerificationA[E, A] = verify(_ => e)
}

trait ToVerificationAOps {

  implicit def ToVerificationOps[A](a: A) = new VerificationAOps[A] {
    def self: A = a
  }
}
