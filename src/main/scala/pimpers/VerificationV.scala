package pimpers

import scalaz._
import Scalaz._
import scala.language.implicitConversions

trait VerificationV[E, A] {

  def verify(e: E)(test: Test[A]): VerificationV[E, A] = verify(_ => e)(test)

  def verify(e: A => E)(test: Test[A]) = this match {
    case VerificationFailure(_) => this
    case VerificationSuccess(a, errors) => if (test(a)) this else VerificationSuccess(a, errors :+ e(a))
  }

  def validation: V[NonEmptyList[E], A] = this match {
    case VerificationFailure(e) => e.wrapNel.fail[A]
    case VerificationSuccess(a, errors) => validation(a, errors)
  }

  private def validation(a: A, errors: List[E]) = errors match {
    case Nil => a.success[NonEmptyList[E]]
    case _ => NonEmptyList.nel(errors.head, errors.tail).fail[A]
  }
}

object VerificationV {
}

final case class VerificationSuccess[E, A](a: A, errors: List[E]) extends VerificationV[E, A]

final case class VerificationFailure[E, A](e: E) extends VerificationV[E, A]

trait ToVerificationVOps {

  implicit def ToVerificationVOps[E, A](v: V[E, A]): VerificationV[E, A] = v match {
    case Success(s) => VerificationSuccess(s, Nil)
    case Failure(f) => VerificationFailure(f)
  }
}
