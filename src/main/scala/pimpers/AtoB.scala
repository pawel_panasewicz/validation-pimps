package pimpers

import scalaz._
import Scalaz._

trait AtoB[A,B] {

  def ->(a: A): B
}

object AtoB {

  implicit def ThrowableToString = new AtoB[Throwable, String] {
    def ->(a: Throwable): String = a.toString
  }

  implicit def ThrowableToNELString :AtoB[Throwable, NonEmptyList[String]] = new AtoB[Throwable, NonEmptyList[String]] {
    def ->(a: Throwable): NonEmptyList[String] = NonEmptyList(a.toString)
  }

  def apply[E,A](e: E => A) = new AtoB[E,A] {
    def ->(a: E): A = e(a)
  }

  def throwableToString(mess: String): AtoB[Throwable, String] = new AtoB[Throwable, String] {
    def ->(a: Throwable): String = s"${mess} + ${a.getMessage}"
  }

  def throwableToString(e: Throwable => String): AtoB[Throwable, String] = new AtoB[Throwable, String] {
    def ->(a: Throwable): String = s"${e(a)}"
  }

  def throwableToNelString(e: Throwable => String): AtoB[Throwable, NonEmptyList[String]] = new AtoB[Throwable, NonEmptyList[String]] {
    def ->(a: Throwable): NonEmptyList[String] = NonEmptyList(s"${e(a)}")
  }

}