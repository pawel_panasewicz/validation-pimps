/*
* Copyright (c) 2012 Paweł Panasewicz
*
*    Licensed under the Apache License, Version 2.0 (the "License");
*    you may not use this file except in compliance with the License.
*    You may obtain a copy of the License at
*
*        http://www.apache.org/licenses/LICENSE-2.0
*
*    Unless required by applicable law or agreed to in writing, software
*    distributed under the License is distributed on an "AS IS" BASIS,
*    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*    See the License for the specific language governing permissions and
*    limitations under the License.
*/

package pimpers

import scalaz._
import Scalaz._
import language.implicitConversions

import scala._

object ValidationPlusV {

  def failWhenException[E,A](v: => V[E, A], toE: AtoB[Throwable, E]): Validation[E, A] = try {
    v
  } catch {
    case e: Exception => toE.->(e).failure[A]
  }
}

trait ValidationPlusVOps[E, A] extends syntax.Ops[V[E, A]] {

  def failWhenException(implicit F0: AtoB[Throwable, E]) = ValidationPlusV.failWhenException(self, F0)

  def failWhenException(e: Throwable => E): V[E, A] = ValidationPlusV.failWhenException(self, AtoB(e))

  /**
   * to SpecificString
   * @return error.toString or "SUCCESS"
   */
  def toSS[E, A](v: Validation[E, A]) = v.fold(_.toString, _ => "SUCCESS")

}

trait ToValidationPlusVOps {

  implicit def ValidationPlusVOps[E, A](v: => V[E, A])(implicit ev0: AtoB[Throwable, E]) = new ValidationPlusVOps[E, A] {
    def self = v
    implicit def F0: AtoB[Throwable, E] = ev0
  }
}

//object test extends App with ToValidationPlusVOps {
//
//
//  val v: Validation[S, Int] = 1.success[S]
//
//  println(v.failWhenException)
//
//  println("a".failure[S].failWhenException)
//  println("a".failure[S].failWhenException.failWhenException)
//
//  println({
//    1 / 0
//    1.success[S]
//  }.failWhenException)
//
//  println({
//    1 / 0
//    1.success[S]
//  }.failWhenException.failWhenException)
//
//
//}
//
