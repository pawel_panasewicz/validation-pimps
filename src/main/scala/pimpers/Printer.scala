/*
* Copyright (c) 2012 Paweł Panasewicz
*
*    Licensed under the Apache License, Version 2.0 (the "License");
*    you may not use this file except in compliance with the License.
*    You may obtain a copy of the License at
*
*        http://www.apache.org/licenses/LICENSE-2.0
*
*    Unless required by applicable law or agreed to in writing, software
*    distributed under the License is distributed on an "AS IS" BASIS,
*    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*    See the License for the specific language governing permissions and
*    limitations under the License.
*/

package pimpers

import scalaz._
import Scalaz._
import scala.language.implicitConversions
import java.io.PrintStream
import scala.Console._

object Printer {

  import Console._

  def p[A](a: A, out: PrintStream) = withOut(out)(println(a.toString))

  def pGreen[A](a: A, out: PrintStream) = withOut(out)(println(GREEN + a.toString + RESET))

  def pRed[A](a: A, out: PrintStream) = withOut(out)(println(RED + a.toString + RESET))

  def pYellow[A](a: A, out: PrintStream) = withOut(out)(println(YELLOW + a.toString + RESET))

  def pBlue[A](a: A, out: PrintStream) = withOut(out)(println(BLUE + a.toString + RESET))

  def pMagenta[A](a: A, out: PrintStream) = withOut(out)(println(MAGENTA + a.toString + RESET))

  def pCyan[A](a: A, out: PrintStream) = withOut(out)(println(CYAN + a.toString + RESET))
}


trait PrinterOps[A] extends syntax.Ops[A] {

  implicit def F: PrintStream

  def p = Printer.p(self, F)

  def pGreen = Printer.pGreen(self, F)

  def pRed = Printer.pRed(self, F)

  def pYellow = Printer.pYellow(self, F)

  def pBlue = Printer.pBlue(self, F)

  def pMagenta = Printer.pMagenta(self, F)

  def pCyan = Printer.pCyan(self, F)
}

trait ToPrinterOps {

  implicit def ToPrinterOps[A](a: => A)(implicit ps: PrintStream) = new PrinterOps[A] {
    implicit def F: PrintStream = ps

    def self: A = a
  }
}

trait PrintStreamInstances {

  implicit def out = Console.out
}
