package pimpers

import scalaz._
import Scalaz._
import language.implicitConversions


object ValidationPlusA {

  def failWhenException[E, A](a: => A, toE: AtoB[Throwable, E]) = V.fromTryCatch(a).bimap(toE.->, identity)
}

trait ValidationPlusAOps[A] extends syntax.Ops[A] {

  def failWhenException[E](implicit F1: AtoB[Throwable, E]): V[E, A] = ValidationPlusA.failWhenException(self, F1)

  def failWhenException[E](e: Throwable => E): V[E, A] = ValidationPlusA.failWhenException(self, AtoB(e))
}

trait ToValidationPlusAOps {

  implicit def ValidationPlusAOps[A](a: => A) = new ValidationPlusAOps[A] {
    def self: A = a
  }
}
