package pimpers

trait Types {
  type S = String
  type Test[A] = A => Boolean
  type V[E, A] = scalaz.Validation[E, A]
  val V = scalaz.Validation
  //  implicit def ArrayShow[A]: Show[Array[A]] = new Show[Array[A]] {
  //    def show(a: Array[A]) = {
  //      if(a.length == 0) "[]".toList
  //      else ("["+a.toList.foldLeft("")( (acc:String, curr:A) => acc + curr.toString)+"]").toList
  //    }
  //  }
}
