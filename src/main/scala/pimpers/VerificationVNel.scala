package pimpers

import scalaz._
import Scalaz._
import scala.language.implicitConversions

trait VerificationVNel[E, A] {

  def verify(e: => E)(test: Test[A]): VerificationVNel[E, A] = verify(_ => e)(test)

  def verify(e: A => E)(test: Test[A]) = this match {
    case VerificationFailureNel(_) => this
    case VerificationSuccessNel(a, errors) => if (test(a)) this else VerificationSuccessNel(a, errors :+ e(a))
  }

  def validation: V[NonEmptyList[E], A] = this match {
    case VerificationFailureNel(e) => NonEmptyList.nel(e.head, e.tail).fail[A]
    case VerificationSuccessNel(a, errors) => validation(a, errors)
  }

  private def validation(a: A, errors: List[E]) = errors match {
    case Nil => a.success[NonEmptyList[E]]
    case _ => NonEmptyList.nel(errors.head, errors.tail).fail[A]
  }
}

final case class VerificationSuccessNel[E, A](a: A, errors: List[E]) extends VerificationVNel[E, A]

final case class VerificationFailureNel[E, A](e: List[E]) extends VerificationVNel[E, A]

trait ToVerificationVNelOps {

  implicit def ToVerificationVNelOps[E, A](v: V[NonEmptyList[E], A]): VerificationVNel[E, A] = v match {
    case Success(s) => VerificationSuccessNel(s, Nil)
    case Failure(f) => VerificationFailureNel(f.list)
  }
}

