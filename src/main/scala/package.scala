import pimpers._
import scalaz._
import Scalaz._

package object pimpers extends ToValidationPlusVOps
with Types
with ToValidationPlusAOps
with ToPrinterOps
with PrintStreamInstances
with ToVerificationAOps
with ToVerificationVOps
with ToVerificationVNelOps
with ToLogAroundOps
{}
